﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Digitalware.OpheliaInvoicing.Controllers
{
    public class CustomersController : ApiControllerBase
    {

        public IEnumerable<Models.Customer> Get()
        {
            var customers = DB.Customers.ToList();
            return customers;

        }


    }
}
