﻿using Digitalware.OpheliaInvoicing.Contracts;
using Digitalware.OpheliaInvoicing.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Digitalware.OpheliaInvoicing.Controllers
{
    public class ProductsController : ApiControllerBase
    {

        public IEnumerable<Models.Product> Get()
        {      
            var products = DB.Products.ToList();
         
            return products;
        }

        public void Delete(int id)
        {
            DB.Products.Remove(DB.Products.Single(p => p.Id == id));
            DB.SaveChanges();           
        }
      
    }
}
