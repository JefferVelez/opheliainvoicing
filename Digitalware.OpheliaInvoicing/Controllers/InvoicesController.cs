﻿using Digitalware.OpheliaInvoicing.Contracts;
using Digitalware.OpheliaInvoicing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Digitalware.OpheliaInvoicing.Controllers
{
    public class InvoicesController : ApiControllerBase
    {

        public IEnumerable<Invoice> Get()
        {
            var invoices = DB.Invoices.ToList();

            return invoices;
        }


        public IEnumerable<Invoice> Get(int id)
        {
            var invoices = DB.Invoices.Where(e => e.Id == id);
            return invoices;
        }

        public IHttpActionResult Post(InvoicesContract.Post.Request request)
        {

            try
            {

                var invoice = new Invoice()
                {
                    CustomerId = request.CustomerId,
                    Date = request.Date,
                };

                var productsIds = request.Details.Select(d => d.ProductId).ToArray();
                var products = DB.Products.Where(p => productsIds.Contains(p.Id)).ToList();

                int rowNumber = 0;
                var invoiceDetails = new List<InvoiceDetail>();

                foreach (var detail in request.Details)
                {
                    var product = products.Where(p => p.Id == detail.ProductId).FirstOrDefault();

                    if (product == null)
                    {
                        return BadRequest($"ProductId: '{detail.ProductId}' Not Found");
                    }
                    else if (product.Stock == 5)
                    {
                        return BadRequest($"Min stock reached for product: '{detail.ProductId}'");
                    }
                    else if ((product.Stock - detail.Quantity) < 5)
                    {
                        return BadRequest($"No Stock avail for product: '{detail.ProductId}' Stock: {product.Stock}");
                    }

                    var invoiceDetail = new InvoiceDetail()
                    {
                        Price = product.Price,
                        ProductId = detail.ProductId,
                        Quantity = detail.Quantity,
                        RowNumber = ++rowNumber,
                        TaxValue = product.Price * 0.16m
                    };

                    //adding details
                    invoiceDetails.Add(invoiceDetail);
                    //subtract product stock
                    product.Stock -= invoiceDetail.Quantity;
                }

                //filling invoice header's totals values according invoice's details sum
                invoice.TaxValue = invoiceDetails.Sum(id => id.TaxValue);
                invoice.Quantity = invoiceDetails.Sum(id => id.Quantity);
                invoice.SubTotal = invoiceDetails.Sum(id => id.SubTotal);

                using (var tran = DB.Database.BeginTransaction())
                {
                    DB.Invoices.Add(invoice);
                    DB.SaveChanges();
                    //InvoiceId generated in DB

                    //adding details
                    foreach (var id in invoiceDetails)
                    {
                        id.InvoiceId = invoice.Id;
                        DB.InvoiceDetails.Add(id);
                    }

                    //updating product info.
                    foreach (var product in products)
                    {
                        DB.Entry(product).State = System.Data.Entity.EntityState.Modified;
                    }

                    DB.SaveChanges();
                    tran.Commit();
                }

                return Ok(new { InvoiceId = invoice.Id });

            }
            catch (Exception ex)
            {
                return BadRequest($"Error: '{ex.InnerException.InnerException.Message}'");

            }


        }
    }
}
