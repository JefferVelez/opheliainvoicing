﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Digitalware.OpheliaInvoicing.Controllers
{
    public class ApiControllerBase: ApiController, IDisposable
    {
        public InvoicingDbContext DB { get; }

        public ApiControllerBase()
        {
            DB = new InvoicingDbContext(); 
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            DB.Dispose();
        }

    }
}