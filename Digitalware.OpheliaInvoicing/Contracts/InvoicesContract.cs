﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Digitalware.OpheliaInvoicing.Contracts
{
    public class InvoicesContract
    {

        public class Post
        {
            public class Request
            {
                public DateTime Date { get; set; }
                public int CustomerId { get; set; }
                public List<DetailRequest> Details { get; set; }
                public class DetailRequest
                {
                    public int ProductId { get; set; }
                    public int Quantity { get; set; }

                }
            }
        }
    }
}