﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Configuration;
using Digitalware.OpheliaInvoicing.Models;

namespace Digitalware.OpheliaInvoicing
{

    public class InvoicingDbContext : DbContext
    {
        public InvoicingDbContext() : base(ConfigurationManager.ConnectionStrings["OpheliaInvoicing"].ConnectionString)
        {

        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceDetail> InvoiceDetails { get; set; }

    }
}