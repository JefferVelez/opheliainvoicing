﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Digitalware.OpheliaInvoicing.Models
{
    [Table("InvoiceDetail")]
    public class InvoiceDetail
    {
        [Key, Column(Order = 0)]
        public int InvoiceId { get; set; }

        [Key, Column(Order = 1)]
        public int RowNumber { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public decimal TaxValue { get; set; }

        [NotMapped]
        public decimal SubTotal { get { return Price * Quantity; } }

        [NotMapped]
        public decimal Total { get { return SubTotal + TaxValue; }  }
    }
}